import { Player } from './Player';

export class Team {
    id: number;
    players: Player[];
    name: string;
    level: number;
}
