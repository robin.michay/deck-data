export enum StadiumType {
    Football,
    Bastketball,
}

export class Coordinates {
    latitude: number;
    longitude: number;
}

export class Stadium {
    id: number;
    type: StadiumType;
    coordinates: Coordinates;
}
