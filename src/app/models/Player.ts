import { User } from './User';
import { Match } from './Match';
import { Team } from './Team';

export enum Strengthes {
    Up,
    Down,
    Left,
    Right
}

export class Player {
    id?: number;
    user: User;
    team?: Team;
    level: number;
    strengthes: Strengthes[];
    current_match?: Match;
}
