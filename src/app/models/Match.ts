import { Player } from './Player';
import { Stadium } from './Stadium';

export enum MatchStatus {
    Waiting,
    Playing,
    Played,
    Interrupted,
}

export enum InterruptionReason {
    Abandon,
    NotEnoughPlayers,
}

export class Match {
    id: number;
    team_1?: Player[];
    team_2?: Player[];
    status: MatchStatus;
    interruption_reason?: InterruptionReason;
    stadium: Stadium;
}
