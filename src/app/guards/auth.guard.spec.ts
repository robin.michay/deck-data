import { TestBed, async, inject } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';

import { AuthGuard } from './auth.guard';
import { AuthenticationService } from '../providers/authentication.service';
import { StorageMock } from '../providers/mocks/tests/storage.mock';

describe('AuthGuard', () => {
  let authGuard: AuthGuard;
  let authenticationService: AuthenticationService;
  let storageSpy;

  beforeEach(async () => {
    storageSpy = jasmine.createSpyObj('Storage', ['get', 'set', 'remove']);

    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        { provide: Storage, useValue: new StorageMock() },
        AuthenticationService,
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    authGuard = TestBed.get(AuthGuard);
    authenticationService = TestBed.get(AuthenticationService);
  });

  // it('should ...', inject([AuthGuard], (guard: AuthGuard) => {
  //   expect(guard).toBeTruthy();
  // }));

  it('should detected when authenticated', () => {
    authenticationService.authenticationState = new BehaviorSubject<boolean>(true);
    expect(authGuard.canActivate()).toBe(true);
  });

  it('should detected when not authenticated', () => {
    authenticationService.authenticationState = new BehaviorSubject<boolean>(false);
    expect(authGuard.canActivate()).toBe(false);
  });
});
