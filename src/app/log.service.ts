import { Injectable } from '@angular/core';
import { AuthenticationService } from './providers/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private authService: AuthenticationService) { }

  login(
    credentials: { email: string, password: string }
  ): Promise<{success: boolean, message: string}> {
    return new Promise(async (resolve, reject) => {
      if (credentials.email === 'test' && credentials.password === 'test') {
        await this.authService.login();
        resolve({
          success: true,
          message: 'Utilisateur connecté !'
        });
      } else {
        resolve({
          success: false,
          message: 'Email ou mot de passe invalide.'
        });
      }
    });
  }
}
