import { User } from '../../models/User';

const users: User[] = [
  {
    id: 1,
    first_name: 'Jack',
    last_name: 'Daniel',
    email: 'jack@daniel.fr',
    date_of_birth: ''
  },
  {
    id: 2,
    first_name: 'Vincent',
    last_name: 'Tim',
    email: 'vincent@tim.fr'
  },
  {
    id: 3,
    first_name: 'Norbert',
    last_name: 'Ordet',
    email: 'norbert@ordet.fr'
  },
  {
    id: 4,
    first_name: 'Gollum',
    last_name: 'Dima',
    email: 'gollum@dima.fr'
  },
  {
    id: 5,
    first_name: 'Sabrina',
    last_name: 'Malika',
    email: 'sabrina@malika.fr'
  },
  {
    id: 6,
    first_name: 'Alberto',
    last_name: 'Giuppogani',
    email: 'alberto@giuppogani.fr'
  },
  {
    id: 7,
    first_name: 'Eve',
    last_name: 'Tulipe',
    email: 'eve@tulipe.fr'
  },
  {
    id: 8,
    first_name: 'Danielle',
    last_name: 'Nova',
    email: 'danielle@nova.fr'
  },
  {
    id: 9,
    first_name: 'Bob',
    last_name: 'Mahley',
    email: 'bob@mahley.fr'
  },
  {
    id: 10,
    first_name: 'Mac',
    last_name: 'Byver',
    email: 'mac@byver.fr'
  },
  {
    id: 11,
    first_name: 'Nicole',
    last_name: 'Riley',
    email: 'nicole@riley.fr'
  },
  {
    id: 12,
    first_name: 'Thomas',
    last_name: 'Parts',
    email: 'thomas@parts.fr'
  },
];

export class UserMock {
  get(): User[] {
    return users;
  }

  getOne(id: number = 1): User {
    return users[id - 1];
  }

  getBy(field: string, value: any): User {
    return users.filter(u => u[field] === value)[0] || undefined;
  }

  post(data: User): User {
    return data;
  }

  patch(data: any, id: number): User {
    return { ...this.getOne(id), ...data };
  }

  delete(id: number): User[] {
    return [...users.slice(0, id + 1), ...users.slice(id + 2)];
  }
}
