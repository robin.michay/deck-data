import { Match, MatchStatus } from '../../models/Match';
import { PlayerMock } from './player.mock';
import { StadiumMock } from './stadium.mock';


const matches: Match[] = [
  {
    id: 1,
    team_1: [
      new PlayerMock().getOne(1),
      new PlayerMock().getOne(2),
      new PlayerMock().getOne(3),
      new PlayerMock().getOne(4),
      new PlayerMock().getOne(5)],
    status: MatchStatus.Waiting,
    stadium: new StadiumMock().getOne(1)
  },
];

export class MatchMock {
  get(): Match[] {
    return matches;
  }

  getOne(id: number = 1): Match {
    return matches[id - 1];
  }

  post(data: Match): Match {
    return data;
  }

  patch(data: any, id: number): Match {
    return { ...this.getOne(id), ...data };
  }

  delete(id: number): Match[] {
    return [...matches.slice(0, id + 1), ...matches.slice(id + 2)];
  }
}
