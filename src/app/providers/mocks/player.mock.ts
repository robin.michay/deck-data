import { Player, Strengthes } from '../../models/Player';
import { UserMock } from './user.mock';

const players: Player[] = [
  {
    id: 1,
    user: new UserMock().getOne(1),
    level: 43,
    strengthes: [Strengthes.Up, Strengthes.Down, Strengthes.Left]
  },
  {
    id: 2,
    user: new UserMock().getOne(2),
    level: 15,
    strengthes: [Strengthes.Up]
  },
  {
    id: 3,
    user: new UserMock().getOne(3),
    level: 22,
    strengthes: [Strengthes.Right, Strengthes.Down]
  },
  {
    id: 4,
    user: new UserMock().getOne(4),
    level: 21,
    strengthes: [Strengthes.Up, Strengthes.Down]
  },
  {
    id: 5,
    user: new UserMock().getOne(5),
    level: 12,
    strengthes: [Strengthes.Left]
  },
  {
    id: 6,
    user: new UserMock().getOne(6),
    level: 6,
    strengthes: [Strengthes.Down]
  },
  {
    id: 7,
    user: new UserMock().getOne(7),
    level: 3,
    strengthes: [Strengthes.Up]
  },
  {
    id: 8,
    user: new UserMock().getOne(8),
    level: 64,
    strengthes: [Strengthes.Up, Strengthes.Down, Strengthes.Left, Strengthes.Right]
  },
  {
    id: 9,
    user: new UserMock().getOne(9),
    level: 33,
    strengthes: [Strengthes.Up, Strengthes.Down]
  },
  {
    id: 10,
    user: new UserMock().getOne(10),
    level: 29,
    strengthes: [Strengthes.Up, Strengthes.Right]
  },
  {
    id: 11,
    user: new UserMock().getOne(11),
    level: 47,
    strengthes: [Strengthes.Up, Strengthes.Left, Strengthes.Right]
  },
  {
    id: 12,
    user: new UserMock().getOne(12),
    level: 12,
    strengthes: [Strengthes.Down]
  },
];

export class PlayerMock {
  get(): Player[] {
    return players;
  }

  getOne(id: number = 1): Player {
    return players[id - 1] || undefined;
  }

  post(data: Player): Player {
    return data;
  }

  patch(data: any, id: number): Player {
    return { ...this.getOne(id), ...data };
  }

  delete(id: number): Player[] {
    return [...players.slice(0, id + 1), ...players.slice(id + 2)];
  }
}
