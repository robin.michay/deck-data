import { Stadium, StadiumType } from 'src/app/models/Stadium';

const stadiums: Stadium[] = [
    {
        id: 1,
        coordinates: {
            latitude: 48.856613,
            longitude: 2.352222,
        },
        type: StadiumType.Bastketball,
    }
];

export class StadiumMock {
  get(): Stadium[] {
    return stadiums;
  }

  getOne(id: number = 1): Stadium {
    return stadiums[id - 1];
  }

  post(data: Stadium): Stadium {
    return data;
  }

  patch(data: any, id: number): Stadium {
    return { ...this.getOne(id), ...data };
  }

  delete(id: number): Stadium[] {
    return [...stadiums.slice(0, id + 1), ...stadiums.slice(id + 2)];
  }
}
