import { Team } from 'src/app/models/Team';
import { PlayerMock } from './player.mock';

const teams: Team[] = [
    {
        id: 1,
        level: 10,
        name: 'La belle équipe',
        players: [
          new PlayerMock().getOne(1),
          new PlayerMock().getOne(2),
          new PlayerMock().getOne(3),
          new PlayerMock().getOne(4),
          new PlayerMock().getOne(5),
          new PlayerMock().getOne(11),
        ]
    },
    {
        id: 2,
        level: 9,
        name: 'L\'autre équipe',
        players: [
          new PlayerMock().getOne(6),
          new PlayerMock().getOne(7),
          new PlayerMock().getOne(8),
          new PlayerMock().getOne(9),
          new PlayerMock().getOne(10),
          new PlayerMock().getOne(12),
        ]
    },
];

export class TeamMock {
  get(): Team[] {
    return teams;
  }

  getOne(id: number = 1): Team {
    return teams[id - 1];
  }

  post(data: Team): Team {
    return data;
  }

  patch(data: any, id: number): Team {
    return { ...this.getOne(id), ...data };
  }

  delete(id: number): Team[] {
    return [...teams.slice(0, id + 1), ...teams.slice(id + 2)];
  }
}
