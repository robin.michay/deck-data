export class StorageMock {
    data = {};

    constructor() {
        this.data = {};
    }

    get<T>(token: string): Promise<T | undefined> {
        // return Promise.resolve(this.data[token]);
        return new Promise((resolve, reject) => {
            resolve(this.data[token] || undefined);
        });
    }

    set(token: string, value: any): Promise<boolean> {
        this.data[token] = value;
        return new Promise((resolve, reject) => {
            resolve(true);
        });
    }

    remove(token: string): Promise<boolean> {
        if (this.data[token]) {
            delete this.data[token];
            return new Promise((resolve, reject) => {
                resolve(true);
            });
        }
        return new Promise((resolve, reject) => {
            resolve(false);
        });
    }
}
