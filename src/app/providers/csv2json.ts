export function csvJSON(csv: string): any[] {
  const lines = csv.split('\n');
  const result = [];
  const headers = lines[0].split(',');

  for (let i = 1; i < lines.length; i++) {

    const obj = {};
    const currentline = lines[i].split(',');

    for (let j = 0; j < headers.length; j++) {
      obj[headers[j].replace('"', '').replace('\'', '').replace(/(\r\n. |\n. |\r)/gm, '')] = currentline[j];
    }

    result.push(obj);

  }

  return result; // JavaScript object
  // return JSON.stringify(result); // JSON
}
