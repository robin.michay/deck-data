import { TestBed } from '@angular/core/testing';
import { Platform } from '@ionic/angular';

import { AuthenticationService } from './authentication.service';
import { StorageMock } from './mocks/tests/storage.mock';
import { Storage } from '@ionic/storage';

describe('AuthenticationService', () => {
  let platformReadySpy,
    platformSpy;

  beforeEach(() => {
    platformReadySpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });

    TestBed.configureTestingModule({
      providers: [
        { provide: Storage, useValue: new StorageMock() },
        { provide: Platform, useValue: platformSpy },
      ]
    });
  });

  it('should be created', () => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    expect(service).toBeTruthy();
  });

  it('should initialize the service', async () => {
    TestBed.get(AuthenticationService);
    expect(platformSpy.ready).toHaveBeenCalled();
  });
});
