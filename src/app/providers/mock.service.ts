import { Injectable } from '@angular/core';
import { UserMock } from './mocks/user.mock';
import { MatchMock } from './mocks/match.mock';
import { PlayerMock } from './mocks/player.mock';
import { StadiumMock } from './mocks/stadium.mock';
import { TeamMock } from './mocks/team.mock';

@Injectable({
  providedIn: 'root'
})
export class MockService {

  static mocks = {
    user: new UserMock(),
    match: new MatchMock(),
    player: new PlayerMock(),
    team: new TeamMock(),
    stadium: new StadiumMock(),
  };

  constructor() {
  }

  getMock(url: string):
    UserMock |
    MatchMock |
    PlayerMock |
    TeamMock |
    StadiumMock {
    const { mocks } = MockService;
    switch (url) {
      case '/users/':
        return mocks.user;
      case '/matches/':
        return mocks.match;
      case '/players/':
        return mocks.player;
      case '/teams/':
        return mocks.team;
      case '/stadiums/':
        return mocks.player;
      default:
        throw 404;
    }
  }

  request<T>(method: string, url: string, data?: any): Promise<T> {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        // resolve('foo');
        switch (method) {
          case 'GET':
            resolve(this.getMock(url).get());
            break;
          case 'POST':
            resolve(this.getMock(url).post(data));
            break;
          case 'PATCH':
            resolve(this.getMock(url).patch(data, data.id));
            break;
          case 'DELETE':
            resolve(this.getMock(url).delete(data.id));
            break;
          default:
            break;
        }
      }, 300);
    });
  }

  post<T>(url: string, data?: any) {
    return this.request<T>('POST', url, data);
  }

  get<T>(url: string) {
    return this.request<T>('GET', url);
  }

  patch<T>(url: string, data?: any) {
    return this.request<T>('POST', url, data);
  }

  delete<T>(url: string, data?: any) {
    return this.request<T>('POST', url, data);
  }
}
