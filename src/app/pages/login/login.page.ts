import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthenticationService } from '../../providers/authentication.service';
import { LogService } from './../../log.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loading: HTMLIonLoadingElement;
  registerCredentials = { email: '', password: '' };

  constructor(
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private logSvc: LogService
  ) { }

  ngOnInit() {
  }

  // login() {
  //   this.authService.login();
  // }

  async login() {
    await this.showLoading();
    const { email, password } = this.registerCredentials;
    const res = await this.logSvc.login({email: email.toLowerCase().trim(), password});
    if (res.success) {
      this.loading.dismiss();
    } else {
      await this.showError(res.message);
    }
  }

  async showLoading() {
    this.loading = await this.loadingCtrl.create({
      message: 'Connexion en cours...',
      backdropDismiss: true
    });
    return this.loading.present();
  }

  async showError(text: string) {
    this.loading.dismiss();

    const alert = await this.alertCtrl.create({
      header: 'Échec',
      message: text,
      buttons: ['OK']
    });
    return alert.present();
  }
}
