import { Component } from '@angular/core';
import { Deck } from '@deck.gl/core';
import { GeoJsonLayer, TripsLayer, PolygonLayer } from '@deck.gl/layers';
import * as mapboxgl from 'mapbox-gl';
import { AuthenticationService } from 'src/app/providers/authentication.service';
import { csvJSON } from 'src/app/providers/csv2json';

export class PolygonLayerData {
  height: number;
  polygon: number[][];
  color?: number[];
}
  /** Remember points (drawn in google sheets) :
    * A : [longitude - cLon, latitude - 2.5 * cLan]
    * B : [longitude - 2 * cLon, latitude - 1.5 * cLan]
    * C : [longitude - 2 * cLon, latitude + .5 * cLan]
    * D : [longitude - cLon, latitude + 1.5 * cLan]
    * E : [longitude + cLon, latitude + 1.5 * cLan]
    * F : [longitude + 2 * cLon, latitude + .5 * cLan]
    * G : [longitude + 2 * cLon, latitude - 1.5 * cLan]
    * H : [longitude + cLon, latitude - 2.5 * cLan]
    * I : [longitude - cLon / 2, latitude - 1.25 * cLan]
    * J : [longitude - cLon, latitude - .75 * cLan]
    * K : [longitude - cLon, latitude + .25 * cLan]
    * L : [longitude - cLon / 2, latitude + .75 * cLan]
    * M : [longitude + cLon / 2, latitude + .75 * cLan]
    * N : [longitude + cLon, latitude + .25 * cLan]
    * O : [longitude + cLon, latitude - .75 * cLan]
    * P : [longitude + cLon / 2, latitude - 1.25 * cLan]
  */
const createPolygon = (longitude: number, latitude: number, coeffLon = 1, coeffLan = 1) => {
  const cLon = coeffLon * 0.0001;
  const cLan = coeffLan * 0.0001;
  return [
    [longitude - cLon, latitude - 2.5 * cLan], // A
    [longitude - 2 * cLon, latitude - 1.5 * cLan], // B
    [longitude - 2 * cLon, latitude + .5 * cLan], // C
    [longitude - cLon, latitude + 1.5 * cLan], // D
    [longitude + cLon, latitude + 1.5 * cLan], // E
    [longitude + 2 * cLon, latitude + .5 * cLan], // F
    [longitude + 2 * cLon, latitude - 1.5 * cLan], // G
    [longitude + cLon, latitude - 2.5 * cLan], // H
    [longitude - cLon, latitude - 2.5 * cLan], // A'
    // [longitude + cLon / 2, latitude - 1.25 * cLan], // P
    // [longitude + cLon, latitude - .75 * cLan], // O
    // [longitude + cLon, latitude + .25 * cLan], // N
    // [longitude + cLon / 2, latitude + .75 * cLan], // M
    // [longitude - cLon / 2, latitude + .75 * cLan], // L
    // [longitude - cLon, latitude + .25 * cLan], // K
    // [longitude - cLon, latitude - .75 * cLan], // J
    // [longitude - cLon / 2, latitude - 1.25 * cLan], // I
  ];
};

const states = [
  {
    height: 20,
    polygon: createPolygon(2.352222, 48.856613, 20000, 20000)
  }
];

const createParisArround = (total: number, PARIS): any => {
  const obj = {};
  for (let i = 1; i <= total; i++) {
    obj['PARIS ' + (i > 9 ? i : '0' + i)] = PARIS;
  }
  return obj;
};

const citiesGeoloc = {
  RENNES: {
    latitude: 48.117266,
    longitude: -1.677793,
  },
  'RENNES METROPOLE': {
    latitude: 48.117266,
    longitude: -1.677793
  },
  'ILE-ET-VILAINE': {
    latitude: 48.117266,
    longitude: -1.677793
  },
  PARIS: {
    latitude: 48.856614,
    longitude: 2.352222
  },
  ...createParisArround(20, {
    latitude: 48.856614,
    longitude: 2.352222
  })
};

console.log('geolocs', citiesGeoloc);

export const INITIAL_VIEW_STATE = {
  // longitude: -74, // NEW YORK
  // latitude: 40.72,
  // longitude: 2.35, // PARIS
  // latitude: 48.85,
  longitude: 0.0833, // LE TEIL
  latitude: 48.4333,
  zoom: 7.8,
  minZoom: 7.8,
  pitch: 45,
  bearing: 0
};

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  map: mapboxgl.Map;
  deck: Deck;
  type = 'depenses';
  year: number;
  interval: any;
  isPaused = false;
  rennesChanges = 0;
  parisChanges = 0;
  data = {
    depenses: undefined,
    prices: undefined,
    constructions: undefined,
    creations: undefined
  };
  constructor(
    private authService: AuthenticationService,
  ) {
  }

  test(event) {
    console.log(event);
  }

  createMap(): mapboxgl.Map {
    return new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/robhok/cjtxkeqpj2vtx1fmo1gnxy46o',
      // Note: deck.gl will be in charge of interaction and event handling
      interactive: false,
      center: [INITIAL_VIEW_STATE.longitude, INITIAL_VIEW_STATE.latitude],
      zoom: INITIAL_VIEW_STATE.zoom,
      bearing: INITIAL_VIEW_STATE.bearing,
      pitch: INITIAL_VIEW_STATE.pitch,
    });
  }

  createDeck(map: mapboxgl.Map, DATA?: any[]) {
    const _data = [];
    for (const d of DATA) {
      const _d: PolygonLayerData = { height: null, polygon: null };
      const cityData = citiesGeoloc[d.libelle] || citiesGeoloc[d.commune];
      if (cityData) {
        console.log('CITY DATA', cityData);
        _d.polygon = createPolygon(cityData.longitude, cityData.latitude, 100, 100);
        _d.height = (d['ratio_culture_pop']) * 100 ||
          d.ratio_logements_pop * 3000 ||
          +d.ratio_entreprises_pop * 5000 ||
          +d.prix_moyen_m2;
        _d.color = d.ratio_culture_pop ?
          [127, 201, 127] : d.ratio_logements_pop ?
          [190, 174, 212] : d.ratio_entreprises_pop ?
          [253, 192, 134] : d.prix_moyen_m2 ?
          [255, 255, 15] : [74, 80, 87];
        _data.push(_d);
      }
    }
    return new Deck({
      canvas: 'deck-canvas',
      width: '100%',
      height: '100%',
      initialViewState: INITIAL_VIEW_STATE,
      controller: true,
      onViewStateChange: ({ viewState }) => {
        map.jumpTo({
          center: [viewState.longitude, viewState.latitude],
          zoom: viewState.zoom,
          bearing: viewState.bearing,
          pitch: viewState.pitch
        });
      },
      layers: _data.map((d, i) => this.createPolygonLayer(d, i))
    });
  }

  startsYearIncrement() {
    this.isPaused = false;
    this.interval = setInterval(() => {
      this.year = (2008 + ((+this.year % 2008) + 1) % 11) ;
      this.addData(+this.year, this.type);
    }, 2000);
  }

  stopsYearIncrement() {
    clearInterval(this.interval);
    this.interval = null;
    this.isPaused = true;
  }

  createPolygonLayer(data: PolygonLayerData, index = 0): PolygonLayer {
    return new PolygonLayer({
      id: 'buildings-' + index,
      data: [data],
      extruded: true,
      wireframe: false,
      fp64: true,
      opacity: 1,
      pickable: true, // important to work
      onHover: info => console.log('info', info),
      onClick: (a) => console.log('click', a),
      onDragStart: (a) => console.log('drag', a),
      onDragEnd: (a) => console.log('dragend', a),
      getPolygon: f => f.polygon,
      getElevation: f => f.height,
      getFillColor: data.color || [74, 80, 87],
    });
  }

  updateDeck(deck: Deck, DATA: any[]) {
    const _data = [];
    for (const d of DATA) {
      const _d: PolygonLayerData = { height: null, polygon: null };
      const cityData = citiesGeoloc[d.libelle] || citiesGeoloc[d.commune] || citiesGeoloc[d.libelle_commune];
      if (cityData) {
        console.log('CITY DATA', cityData);
        _d.polygon = createPolygon(cityData.longitude, cityData.latitude, 100, 100);
        _d.height = (d['ratio_culture_pop']) * 100 ||
          d.ratio_logements_pop * 3000 ||
          +d.ratio_entreprises_pop * 5000 ||
          +d.prix_moyen_m2;
        _d.color = d.ratio_culture_pop ?
          [127, 201, 127] : d.ratio_logements_pop ?
          [190, 174, 212] : d.ratio_entreprises_pop ?
          [253, 192, 134] : d.prix_moyen_m2 ?
          [255, 255, 15] : [74, 80, 87];
        _data.push(_d);
      }
    }
    deck.setProps({
      layers: _data.map((d, i) => this.createPolygonLayer(d, i))
    });
  }

  async ionViewWillEnter() {
    // Set your mapbox token here
    Object.assign(mapboxgl, {
      accessToken: 'pk.eyJ1Ijoicm9iaG9rIiwiYSI6ImNpcTE5djVkMDAwMnBoc20yYTNuaTdoOHUifQ.ihGqP7NO2XJv97AhsBONZg'
    });
    this.map = this.createMap();

    await this.addData(2008, this.type);
    this.startsYearIncrement();
  }

  getDataByType(type: string) {
    switch (type) {
      case 'depenses':
        return this.data.depenses;
      case 'prices':
        return this.data.prices;
      case 'constructions':
        return this.data.constructions;
      case 'creations':
        return this.data.creations;
    }
  }

  getPropertyByType(type: string) {
    switch (type) {
      case 'depenses':
        return 'ratio_culture_pop';
      case 'prices':
        return 'prix_moyen_m2';
      case 'constructions':
        return 'ratio_logements_pop';
      case 'creations':
        return 'ratio_entreprises_pop';
    }
  }

  getParis(data: any[]) {
    try {
      return data.filter(d =>
        (d.libelle_commune && d.libelle_commune.indexOf('PARIS') !== -1) ||
        (d.libelle && d.libelle.indexOf('PARIS') !== -1) ||
        (d.commune && d.commune.indexOf('PARIS') !== -1)
      )[0] || undefined;
    } catch (e) {
      return undefined;
    }
  }

  getRennes(data: any[]) {
    try {
      return data.filter(d =>
        (d.libelle_commune && d.libelle_commune.indexOf('RENNES') !== -1) ||
        (d.libelle && d.libelle.indexOf('RENNES') !== -1) ||
        (d.commune && d.commune.indexOf('RENNES') !== -1)
      )[0] || undefined;
    } catch (e) {
      return undefined;
    }
  }

  async addData(year: number, type: string) {
    const _year = year.toString();
    console.log(_year);
    if (!this.data.depenses) {
      this.data.depenses = await this.getDepensesData(_year);
    }
    if (!this.data.prices) {
      this.data.prices = await this.getHousePricing(_year);
    }
    if (!this.data.constructions) {
      this.data.constructions = await this.getHouseConstruction(_year);
    }
    if (!this.data.creations) {
      this.data.creations = await this.getCompaniesCreation(_year);
    }
    const current = this.getDataByType(type).filter(d => d.annee && d.annee === _year);
    if (!this.deck) {
      this.deck = this.createDeck(this.map, current);
    } else {
      // console.log('update', current);
      this.updateDeck(this.deck, current);
    }
    // GET CHANGES
    const old = this.getDataByType(type).filter(d => d.annee && d.annee === '' + (+_year - 1));
    const currentRennes = this.getRennes(current);
    const oldRennes = this.getRennes(old);
    const currentParis = this.getParis(current);
    const oldParis = this.getParis(old);
    const property = this.getPropertyByType(type);
    if (currentParis && currentParis[property] && oldParis && oldParis[property]) {
      this.parisChanges = (currentParis[property] * 100) / oldParis[property] - 100;
      this.parisChanges = +this.parisChanges.toFixed(2);
    }
    if (currentRennes && currentRennes[property] && oldRennes && oldRennes[property]) {
      this.rennesChanges = (currentRennes[property] * 100) / oldRennes[property] - 100;
      this.rennesChanges = +this.rennesChanges.toFixed(2);
    }
    console.log('old', old);
    console.log('current', current);
    console.log(oldParis, currentParis);
    console.log(oldRennes, currentRennes);
    this.year = +_year;
  }

  async getDepensesData(year: string) {
    const depenses = await (await fetch('/assets/depenses.csv')).text();
    return csvJSON(depenses);
  }

  async getHousePricing(year: string) {
    const prices = await (await fetch('/assets/evolution_px_immobilier.csv')).text();
    return csvJSON(prices);
  }

  async getHouseConstruction(year: string) {
    const constructions = await (await fetch('/assets/evolution_construction_logements.csv')).text();
    return csvJSON(constructions);
  }

  async getCompaniesCreation(year: string) {
    const creations = await (await fetch('/assets/evolution_creations_entreprises.csv')).text();
    return csvJSON(creations);
  }
}
