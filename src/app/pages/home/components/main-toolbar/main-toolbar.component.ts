import { Component, Input } from '@angular/core';

@Component({
  selector: 'main-toolbar',
  templateUrl: 'main-toolbar.component.html',
  styleUrls: ['main-toolbar.component.scss'],
})
export class MainToolbar {
  @Input() title: string;
  constructor() {}
}
